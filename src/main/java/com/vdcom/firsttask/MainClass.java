package com.vdcom.firsttask;

public class MainClass {

    private static final String BORDER = "----------------";

    public static void main(String[] args) {
        long inputNumber = parseInputParameter(args);
        Decision.firstDecision(inputNumber);
        System.out.println(BORDER);
        Decision.secondDecision(inputNumber);
        System.out.println(BORDER);
        Decision.thirdDecision(inputNumber);
    }

    private static long parseInputParameter(String[] args) {
        if (args.length != 1)
            throw new RuntimeException("Incorrect number of parameters...");
        long inputNumber;
        try {
            inputNumber = Integer.parseInt(args[0]);
            if (inputNumber < 1)
                throw new RuntimeException("Incorrect input number...");
            return inputNumber;
        } catch (NumberFormatException ex) {
            throw new RuntimeException("Incorrect input number...");
        }
    }

}
