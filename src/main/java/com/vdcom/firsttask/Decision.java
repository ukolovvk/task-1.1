package com.vdcom.firsttask;

import java.util.stream.LongStream;

public class Decision {

    public static void firstDecision(long inputNumber) {
        LongStream.rangeClosed(1, inputNumber).forEach(Decision::numberHandle);
    }

    public static void secondDecision(long inputNumber) {
        for (long currentNumber = 1; currentNumber <= inputNumber; currentNumber++)
            numberHandle(currentNumber);
    }

    public static void thirdDecision(long inputNumber) {
        LongStream.rangeClosed(1, inputNumber).forEach(number -> {
            int remainder = (int) (number % 15);
            switch (remainder) {
                case 3,6,9,12 -> System.out.println("Foo");
                case 5,10 -> System.out.println("Bar");
                case 0 -> System.out.println("FooBar");
                default -> System.out.println(number);
            }
        });
    }

    private static void numberHandle(long number) {
        boolean isMultipleOfThree = number % 3 == 0;
        boolean isMultipleOfFive = number % 5 == 0;
        if (isMultipleOfThree) System.out.print("Foo");
        if (isMultipleOfFive) System.out.print("Bar");
        if (!isMultipleOfThree && !isMultipleOfFive) System.out.print(number);
        System.out.println();
    }
}
