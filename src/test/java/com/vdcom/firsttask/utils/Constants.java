package com.vdcom.firsttask.utils;

public class Constants {

    public static final String ETALON_FOR_20 = """
            1
            2
            Foo
            4
            Bar
            Foo
            7
            8
            Foo
            Bar
            11
            Foo
            13
            14
            FooBar
            16
            17
            Foo
            19
            Bar
            """.replace("\n", "\r\n");
}
