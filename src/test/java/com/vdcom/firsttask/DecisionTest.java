package com.vdcom.firsttask;

import com.vdcom.firsttask.utils.Constants;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class DecisionTest {

    private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private PrintStream originalOutStream = System.out;
    private final long TEST_INPUT = 20;

    @BeforeEach
    public void setUpOutStream() {
        System.setOut(new PrintStream(byteArrayOutputStream));
    }

    @AfterEach
    public void setUpOriginalStream() {
        System.setOut(originalOutStream);
    }

    @Test
    public void firstDecisionTest() {
        Decision.firstDecision(TEST_INPUT);
        Assertions.assertEquals(Constants.ETALON_FOR_20, byteArrayOutputStream.toString());
    }

    @Test
    public void secondDecisionTest() {
        Decision.secondDecision(TEST_INPUT);
        Assertions.assertEquals(Constants.ETALON_FOR_20, byteArrayOutputStream.toString());
    }

    @Test
    public void thirdDecisionTest() {
        Decision.thirdDecision(TEST_INPUT);
        Assertions.assertEquals(Constants.ETALON_FOR_20, byteArrayOutputStream.toString());
    }
}
